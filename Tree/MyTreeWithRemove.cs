﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    public class MyTree<Tkey, TVal> where Tkey : IComparable
    {
        public delegate void ForEachDelegate(KeyValuePair<Tkey, TVal> pair);

        private class TreeItem
        {
            public KeyValuePair<Tkey, TVal> _pair;
            public TreeItem _parent;
            public TreeItem _left;
            public TreeItem _right;
            public TreeItem(Tkey key, TVal value, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = new KeyValuePair<Tkey, TVal>(key, value);
                _parent = parent;
                _left = left;
                _right = right;
            }
            public TreeItem(KeyValuePair<Tkey, TVal> pair, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = pair;
                _parent = parent;
                _left = left;
                _right = right;
            }
        }

        private TreeItem _root = null;
        private int _counter = 0;
        private bool _allowDuplicateKeys;

        public MyTree(bool allowDuplicateKeys = false)
        {
            _allowDuplicateKeys = allowDuplicateKeys;
        }

        public void Add(Tkey key, TVal value)
        {
            if (_root == null)
            {
                _root = new TreeItem(key, value);
                ++_counter;
            }
            else
            {
                Add(new KeyValuePair<Tkey, TVal>(key, value), _root);
            }
        }

        private void Add(KeyValuePair<Tkey, TVal> pair, TreeItem item)
        {
            if (!_allowDuplicateKeys && pair.Key.CompareTo(item._pair.Key) == 0)
            {
                item._pair = pair;
            }
            else if (pair.Key.CompareTo(item._pair.Key) < 0)//Go to left
            {
                if (item._left == null)
                {
                    item._left = new TreeItem(pair, item);
                    ++_counter;
                }
                else
                {
                    Add(pair, item._left);
                }
            }
            else//go to right
            {
                if (item._right == null)
                {
                    item._right = new TreeItem(pair, item);
                    ++_counter;
                }
                else
                {
                    Add(pair, item._right);
                }
            }
        }
        public void ForEach(ForEachDelegate d)
        {
            if (_root != null)
            {
                ForEach(d, _root);
            }
        }
        private void ForEach(ForEachDelegate d, TreeItem item)
        {
            if (item._left != null)
            {
                ForEach(d, item._left);
            }

            d.Invoke(item._pair);

            if (item._right != null)
            {
                ForEach(d, item._right);
            }
        }

        private void RemoveItem(TreeItem item, TreeItem parent)
        {
            if (item._left==null && item._right == null)
            {
                RemoveItemWihtoutChildren(item, parent);
            }
            else if (item._left != null && item._right != null)
            {
                RemoveItemWihtBothChildren(item, parent);
            }
            else
            {
                RemoveItemWihtOneChild(item, parent);
            }
            --_counter;
        }

        private void RemoveItemWihtoutChildren(TreeItem item, TreeItem parent)
        {
            if (item == _root)
            {
                _root = null;
                return;
            }
            if (parent._left == item)
            {
                parent._left = null;
            }
            else
            {
                parent._right = null;
            }
        }

        private void RemoveItemWihtOneChild(TreeItem item, TreeItem parent)
        {
            if (item == _root)
            {
                if (item._left != null)
                {
                    _root = item._left;
                    _root._parent = null;
                    return;
                }
                else if (item._right != null)
                {
                    _root = item._right;
                    _root._parent = null;
                    return;
                }
            }
            else
            {
                if (parent._left == item)
                {
                    if (item._right != null)
                    {
                        parent._left = item._right;
                    }
                    else if (item._left != null)
                    {
                        parent._left = item._left;
                    }
                }
                else
                {
                    if (item._right != null)
                    {
                        parent._right = item._right;
                    }
                    else if (item._left != null)
                    {
                        parent._right = item._left;
                    }
                }
            }
            
        }

        private void RemoveItemWihtBothChildren(TreeItem item, TreeItem parent)
        {
            //Firn sucseccor Node
            TreeItem sucsessor = item._right;
            TreeItem sucsessorParent = item;
            {
                while(sucsessor._left != null)
                    {
                    sucsessorParent = sucsessor;
                    sucsessor = sucsessor._left;
                }
            }
            item._pair = sucsessor._pair;
            //replace value deleted node and sucsessor
            sucsessorParent._left = sucsessor._right;
        }
    }
}

