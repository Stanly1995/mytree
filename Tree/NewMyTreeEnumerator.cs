﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    public class NewMyTreeDelegate<Tkey, TVal> where Tkey : IComparable
    {
        public delegate void ForEachDelegate(KeyValuePair<Tkey, TVal> pair);

        private class TreeItem
        {
            public KeyValuePair<Tkey, TVal> _pair;
            public TreeItem _parent;
            public TreeItem _left;
            public TreeItem _right;
            public TreeItem(Tkey key, TVal value, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = new KeyValuePair<Tkey, TVal>(key, value);
                _parent = parent;
                _left = left;
                _right = right;
            }
            public TreeItem(KeyValuePair<Tkey, TVal> pair, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = pair;
                _parent = parent;
                _left = left;
                _right = right;
            }
        }

        private TreeItem _root = null;
        private int _counter = 0;
        private bool _allowDuplicateKeys;

        public NewMyTreeDelegate(bool allowDuplicateKeys = false)
        {
            _allowDuplicateKeys = allowDuplicateKeys;
        }

        public void Add(Tkey key, TVal value)
        {
            if (_root == null)
            {
                _root = new TreeItem(key, value);
                ++_counter;
            }
            else
            {
                Add(new KeyValuePair<Tkey, TVal>(key, value), _root);
            }
        }

        private void Add(KeyValuePair<Tkey, TVal> pair, TreeItem item)
        {
            if (!_allowDuplicateKeys && pair.Key.CompareTo(item._pair.Key) == 0)
            {
                item._pair = pair;
            }
            else if (pair.Key.CompareTo(item._pair.Key) < 0)//Go to left
            {
                if (item._left == null)
                {
                    item._left = new TreeItem(pair, item);
                    ++_counter;
                }
                else
                {
                    Add(pair, item._left);
                }
            }
            else//go to right
            {
                if (item._right == null)
                {
                    item._right = new TreeItem(pair, item);
                    ++_counter;
                }
                else
                {
                    Add(pair, item._right);
                }
            }
        }
        public void ForEach(ForEachDelegate d)
        {
            if (_root != null)
            {
                ForEach(d, _root);
            }
        }
        private void ForEach(ForEachDelegate d, TreeItem item)
        {
            if (item._left != null)
            {
                ForEach(d, item._left);
            }

            d.Invoke(item._pair);

            if (item._right != null)
            {
                ForEach(d, item._right);
            }
        }

        public IEnumerator<KeyValuePair<Tkey, TVal>> GetEnumerator()
        {
            using (IEnumerator<TreeItem> e = GetTreeItemEnumerator(_root))
            {
                while (e.MoveNext())
                {
                    yield return e.Current._pair;
                }
            }
        }
        private IEnumerator<TreeItem> GetTreeItemEnumerator(TreeItem item)
        {
            Stack<TreeItem> itemStack = new Stack<TreeItem>();
            while (item != null || itemStack.Count != 0)
            {
                if (itemStack.Count != 0)
                {
                    item = itemStack.Pop();
                    yield return item;
                    if (item._right != null)
                    {
                        item = item._right;
                    }
                    else
                    {
                        item = null;
                    }
                }
                while (item != null)
                {
                    itemStack.Push(item);
                    item = item._left;
                }
            }
        }

        private IEnumerator<Tuple<TreeItem, TreeItem>> GetTreePair(TreeItem item)
        {
            Stack<TreeItem> itemStack = new Stack<TreeItem>();
            TreeItem parent;
            while (item != null || itemStack.Count != 0)
            {
                if (itemStack.Count != 0)
                {
                    item = itemStack.Pop();
                    parent = itemStack.Peek();
                    Tuple<TreeItem, TreeItem> pair = new Tuple<TreeItem, TreeItem>(item, parent);
                    yield return pair;
                    if (item._right != null)
                    {
                        item = item._right;
                    }
                    else
                    {
                        item = null;
                    }
                }
                while (item != null)
                {
                    itemStack.Push(item);
                    item = item._left;
                }
            }
        }
    }
}

