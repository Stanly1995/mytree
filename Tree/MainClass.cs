﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    /*
     * 1. Если у элемента есть левый потомок, то идем к левому потомку.
     * 2. Обрабатываем текущее значение.
     * 3. Если есть правый потомок, опрабатываем правое значение
     * */
    class MainClass
    {
        static void Test(KeyValuePair<int, int> pair)
        {
            Console.WriteLine($"[{pair.Key}] = {pair.Value}");
        }
        static void Main(string[] args)
        {
            NewMyTreeWithoutRecursion<int, int> tree = new NewMyTreeWithoutRecursion<int, int>();
            Random rnd = new Random();
            for (int i=0; i<100; ++i)
            {
                tree.AddRecursionLess(new KeyValuePair<int, int>(rnd.Next(100), rnd.Next(100)));
            }
            tree.ForEach(Test);
            Console.ReadKey();
        }
    }
}
